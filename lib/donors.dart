import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import './models/donor.dart';

class Donors extends StatefulWidget {
  final String bloodType;
  final String searchLocation;
  final String searchType;
  final Function goBack;

  Donors({this.goBack, this.bloodType, this.searchLocation, this.searchType});

  @override
  _DonorsState createState() => _DonorsState();
}

class _DonorsState extends State<Donors> {
  Future<List> _getDonors() async {
    const url = 'http://aieshs.com/blood_donation/searchDonors.php';
    var response = await http.post(url, body: {
      'bloodType': widget.bloodType,
      'location': widget.searchLocation,
      'type': widget.searchType
    });
    if (response.statusCode == 200) {
      print(response.body);

      return json
          .decode(response.body)
          .map((data) => Donor.fromJson(data))
          .toList();
    } else {
      throw Exception('Failed to load post');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 15, left: 15, right: 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                InkWell(
                  child: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                    size: 42,
                  ),
                  onTap: () {
                    widget.goBack();
                  },
                ),
                SizedBox(width: 20),
                Text(
                  'Donors List',
                  style: TextStyle(
                    fontSize: 42,
                  ),
                )
              ],
            ),
            FutureBuilder<List>(
              future: _getDonors(),
              builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                Widget children;

                if (snapshot.hasData) {
                  children = Expanded(
                    child: DonorsList(snapshot.data),
                  );
                } else {
                  children = SizedBox(
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                    ),
                    width: 60,
                    height: 60,
                  );
                }
                return Container(
                  child: children,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class DonorsList extends StatelessWidget {
  final List<dynamic> _donorsList;

  DonorsList(this._donorsList);

  void _callDonor(String phone) async{
    final phoneNumber = 'tel: $phone';
    if (await canLaunch(phoneNumber)) {
      await launch(phoneNumber);
    } else {
      throw 'Could not Call Phone';
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _donorsList.length,
      itemBuilder: (BuildContext context, int index) {
        Donor dnr = _donorsList[index];
        return Card(
          elevation: 5,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                  bottom: 10,
                  left: 20,
                  right: 20,
                ),
                height: 70,
                width: 70,
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    border: Border.all(color: Colors.white54)),
                child: Center(
                  child: Text(
                    dnr.bloodType,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 26,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Center(
                            child: Text(
                              dnr.name,
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            child: Text('City: ${dnr.location}'),
                          ),
                          Container(
                            child: RaisedButton.icon(
                              elevation: 5,
                              color: Colors.red,
                              icon: Icon(
                                Icons.phone,
                                color: Colors.white,
                              ),
                              label: Text(
                                'Call',
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: (){
                                _callDonor(dnr.contact);
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}