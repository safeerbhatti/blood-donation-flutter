import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './widgets/blood_type_list.dart';
import './widgets/location_list.dart';
import './widgets/success_dialog.dart';
import './models/user.dart';
import './models/donor.dart';

class MakeRequest extends StatefulWidget {
  @override
  _MakeRequestState createState() => _MakeRequestState();
}

class _MakeRequestState extends State<MakeRequest> {
  var selectedBloodType;
  var selectedLocation = "Hyderabad";
  DateTime selectedDate = DateTime.now();

  BloodTypeList listController;
  LocationList locationController;

  bool isLoading = false;

  void selectBloodType(var type) {
    selectedBloodType = type;
  }

  void selectLocation(String location) {
    selectedLocation = location;
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  bool validateForm() {
    if (selectedBloodType == null) return false;
    return true;
  }

  void _sendRequest() async {
    setState(() {
      isLoading = true;
    });

    if (validateForm() == true) {
      Donor usr = await User.getUser();
      const url = 'http://aieshs.com/blood_donation/addRequest.php';
      var response = await http.post(url, body: {
        'uid': usr.id,
        'bloodType': selectedBloodType,
        'location': selectedLocation,
        'ondate': "${selectedDate.toLocal()}".split(' ')[0],
      });

      if (response.statusCode == 200) {
        showDialog(
          context: context,
          builder: (_) => Image.asset(
            'assets/images/request_sent.jpg',
            fit: BoxFit.fill,
          ),
        )
        .whenComplete(setLoadingOff);

        setState(() {
          selectedBloodType = null;
          locationController.selectedLocation = 'Hyderabad';
          listController.selectedType = null;
        });
      } else {
        // If that call was not successful, throw an error.
        throw Exception('Failed to load post');
      }
    } else {
      AlertDialog _showDialog = AlertDialog(
        title: Text('Error'),
        content: Text('Please Complete Form'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text('Okay'),
          )
        ],
      );

      showDialog(context: context, child: _showDialog).whenComplete(setLoadingOff);
    }
  }

  void setLoadingOff() {
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedSwitcher(
        duration: const Duration(milliseconds: 300),
        child: isLoading
            ? Center(
                child: SizedBox(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                  ),
                  width: 60,
                  height: 60,
                ),
              )
            : Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 15, left: 10, right: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Request for Blood',
                      style:
                          TextStyle(fontSize: 26, fontWeight: FontWeight.w700),
                    ),
                    Text(
                      'Request Blood whenever wherever you need',
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Text(
                        'Choose Blood Group',
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.grey,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    listController = BloodTypeList(selectBloodType),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Column(
                        children: <Widget>[
                          Text(
                            'Location',
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.grey,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.start,
                          ),
                        ],
                      ),
                    ),
                    locationController = LocationList(selectLocation),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 15),
                            child: Text(
                              'Date',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start,
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                _selectDate(context);
                              },
                              child: Container(
                                padding: EdgeInsets.only(top: 10),
                                decoration: BoxDecoration(
                                  border: new Border(
                                    bottom:
                                        new BorderSide(color: Colors.black12),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    "${selectedDate.toLocal()}".split(' ')[0],
                                    style: TextStyle(
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 30),
                      child: ButtonTheme(
                        minWidth: (MediaQuery.of(context).size.width / 2) - 20,
                        child: RaisedButton(
                          color: Colors.red,
                          textColor: Colors.white,
                          padding: EdgeInsets.all(20),
                          child: Center(
                            child: Text('Request for Blood'),
                          ),
                          onPressed: () {
                            _sendRequest();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
