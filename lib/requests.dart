import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import './models/donor.dart';
import './models/user.dart';
import './widgets/success_dialog.dart';

class Requests extends StatefulWidget {
  @override
  _RequestsState createState() => _RequestsState();
}

class _RequestsState extends State<Requests> {
  List othersRequest;
  List myRequest;

  bool isLoading = false;

  void _callDonor(String phone) async {
    final phoneNumber = 'tel: $phone';
    if (await canLaunch(phoneNumber)) {
      await launch(phoneNumber);
    } else {
      throw 'Could not Call Phone';
    }
  }

  void _acceptRequest(id) async {
    setState(() {
      isLoading = true;
    });
    Donor usr = await User.getUser();
    const url = 'http://aieshs.com/blood_donation/updateRequest.php';
    var response = await http.post(url, body: {
      'uid': usr.id,
      'request_id': id,
      'status': '1',
    });

    if (response.statusCode == 200) {
      showDialog(
        context: context,
        builder: (_) => SuccessDialog(
          dialogText: 'Your Donation Request Has been Submitted',
        ),
      ).whenComplete(setLoadingOff);
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to accept request');
    }
  }

  void _completeDonation(id) async {
    setState(() {
      isLoading = true;
    });

    const url = 'http://aieshs.com/blood_donation/updateRequest.php';
    var response = await http.post(url, body: {
      'request_id': id,
      'status': '2',
    });

    if (response.statusCode == 200) {
      showDialog(
        context: context,
        builder: (_) => SuccessDialog(
          dialogText: 'Thank you for notifying us!',
        ),
      ).whenComplete(setLoadingOff);
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to complete donation');
    }
  }

  void setLoadingOff() async {
    await _loadLists();
    setState(() {
      isLoading = false;
    });
  }

  Future<bool> _loadLists() async {
    Donor usr = await User.getUser();
    const url = 'http://aieshs.com/blood_donation/requests.php';
    var response = await http.post(url, body: {
      'uid': usr.id,
    });
    if (response.statusCode == 200) {
      var requestHandler = json.decode(response.body);
      othersRequest =
          requestHandler['others'].map((data) => Donor.fromJson(data)).toList();
      myRequest =
          requestHandler['my'].map((data) => Donor.fromJson(data)).toList();
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 300),
      child: isLoading
          ? Center(
              child: SizedBox(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                ),
                width: 60,
                height: 60,
              ),
            )
          : FutureBuilder(
              future: _loadLists(),
              builder: (ctx, snapShot) {
                Widget childWidget;

                if (snapShot.hasData) {
                  childWidget = DefaultTabController(
                    length: 2,
                    child: new Scaffold(
                      appBar: new PreferredSize(
                        preferredSize: Size.fromHeight(kToolbarHeight * 2),
                        child: new Container(
                          color: Colors.white,
                          child: new SafeArea(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(20.0),
                                  child: Text(
                                    'Requests for Blood',
                                    style: TextStyle(
                                        fontSize: 26,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                                new Expanded(child: new Container()),
                                new TabBar(
                                  labelColor: Colors.black,
                                  indicatorColor: Colors.red,
                                  indicatorWeight: 3,
                                  tabs: [
                                    new Text("Other`s Requests"),
                                    new Text("My Requests")
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      body: new TabBarView(
                        children: <Widget>[
                          OthersRequest(
                            othersRequest,
                            _acceptRequest,
                            _callDonor,
                          ),
                          MyRequests(
                            myRequest,
                            _completeDonation,
                            _callDonor,
                          ),
                        ],
                      ),
                    ),
                  );
                } else {
                  childWidget = Center(
                    child: SizedBox(
                      child: CircularProgressIndicator(
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(Colors.red),
                      ),
                      width: 60,
                      height: 60,
                    ),
                  );
                }

                return childWidget;
              },
            ),
    );
  }
}

class OthersRequest extends StatelessWidget {
  final List<dynamic> _donorsList;
  final Function callDonor;
  final Function updateRequest;

  OthersRequest(this._donorsList, this.updateRequest, this.callDonor);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: _donorsList.length,
      itemBuilder: (BuildContext context, int index) {
        Donor dnr = _donorsList[index];
        return Container(
          margin: EdgeInsets.only(left: 10, right: 10, top: 10),
          child: Card(
            elevation: 5,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    top: 10,
                    bottom: 10,
                    left: 10,
                  ),
                  height: 70,
                  width: 70,
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      border: Border.all(color: Colors.white54)),
                  child: Center(
                    child: Text(
                      dnr.bloodType,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 26,
                      ),
                    ),
                  ),
                ),
                Flexible(
                  flex: 6,
                  child: Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Center(
                              child: Text(
                                dnr.name,
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              child: Text('City: ${dnr.location}'),
                            ),
                            Container(
                              child: Text('Date: ${dnr.onDate}'),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Flexible(
                  fit: FlexFit.tight,
                  flex: 4,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          border:
                              Border(bottom: BorderSide(color: Colors.white)),
                        ),
                        height: 45,
                        child: RaisedButton.icon(
                          elevation: 5,
                          color: Colors.red,
                          icon: Icon(
                            Icons.phone,
                            color: Colors.white,
                          ),
                          label: Text(
                            'Call',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            callDonor(dnr.contact);
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          border: Border(top: BorderSide(color: Colors.white)),
                        ),
                        height: 45,
                        child: RaisedButton.icon(
                          elevation: 5,
                          color: Colors.red,
                          icon: Icon(
                            Icons.favorite,
                            color: Colors.white,
                          ),
                          label: Text(
                            'Donate',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            updateRequest(dnr.id);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class MyRequests extends StatelessWidget {
  final List<dynamic> _donorsList;
  final Function callDonor;
  final Function updateRequest;

  MyRequests(this._donorsList, this.updateRequest, this.callDonor);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: _donorsList.length,
      itemBuilder: (BuildContext context, int index) {
        Donor dnr = _donorsList[index];
        return Container(
          margin: EdgeInsets.only(left: 10, right: 10, top: 10),
          child: Card(
            elevation: 5,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    top: 10,
                    bottom: 10,
                    left: 10,
                  ),
                  height: 70,
                  width: 70,
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      border: Border.all(color: Colors.white54)),
                  child: Center(
                    child: Text(
                      dnr.bloodType,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 26,
                      ),
                    ),
                  ),
                ),
                Flexible(
                  flex: 6,
                  child: Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Center(
                              child: dnr.status == 0
                                  ? Text(
                                      'Pending',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    )
                                  : Column(
                                      children: <Widget>[
                                        Text('Accepted By'),
                                        Text(
                                          dnr.donorName,
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        )
                                      ],
                                    ),
                            )
                          ],
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              child: Text('${dnr.location} / ${dnr.onDate}'),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Flexible(
                  fit: FlexFit.tight,
                  flex: 4,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          border:
                              Border(bottom: BorderSide(color: Colors.white)),
                        ),
                        height: 45,
                        child: RaisedButton.icon(
                          elevation: 5,
                          color: Colors.red,
                          icon: Icon(
                            Icons.phone,
                            color: Colors.white,
                          ),
                          label: Text(
                            'Call',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            callDonor(dnr.contact);
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          border: Border(top: BorderSide(color: Colors.white)),
                        ),
                        height: 45,
                        child: RaisedButton.icon(
                          elevation: 5,
                          color: Colors.red,
                          icon: Icon(
                            Icons.favorite,
                            color: Colors.white,
                          ),
                          label: Text(
                            dnr.status != 2
                                ? 'Mark as\nDonated'
                                : 'Marked as\nDonated',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                          onPressed: () {
                            updateRequest(dnr.id);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
