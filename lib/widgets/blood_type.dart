import 'package:flutter/material.dart';

class BloodType extends StatelessWidget {
  final String bloodType;
  final String selectedType;
  final Function changeBloodType;

  BloodType(this.bloodType, this.selectedType, this.changeBloodType);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 5,
      animationDuration: const Duration(milliseconds: 300),
      color: bloodType == selectedType?Colors.red:Colors.white,
      textColor: bloodType == selectedType?Colors.white:Colors.black,
      child: Center(
        child: Text(bloodType),
      ),
      onPressed: (){
        changeBloodType();
      },
    );
  }
}
