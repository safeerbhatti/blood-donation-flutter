import 'package:flutter/material.dart';

import '../models/blood_types.dart';
import 'blood_type.dart';

class BloodTypeList extends StatefulWidget {

  final Function selectBloodType;
  String selectedType;

  BloodTypeList(this.selectBloodType);

  @override
  _BloodTypeListState createState() => _BloodTypeListState();
}

class _BloodTypeListState extends State<BloodTypeList> {

  void changeBloodType(type){
    setState(() {
      widget.selectedType = type;
      widget.selectBloodType(type);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView(
        shrinkWrap: true,
        children: bloodTypes.map((bt) => BloodType(bt, widget.selectedType, () => changeBloodType(bt) )).toList(),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 85,
          crossAxisSpacing: 20,
          mainAxisSpacing: 10,
        ),
      ),
    );
  }
}
