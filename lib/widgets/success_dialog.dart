import 'package:flutter/material.dart';

class SuccessDialog extends StatefulWidget {
  final String dialogText;
  final String dialogImage;

  SuccessDialog({this.dialogText, this.dialogImage});

  @override
  State<StatefulWidget> createState() => _SuccessDialogState();
}

class _SuccessDialogState extends State<SuccessDialog>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: widget.dialogImage == null
              ? _TextDialog(widget.dialogText)
              : _ImageDialog(widget.dialogImage),
        ),
      ),
    );
  }
}

class _ImageDialog extends StatelessWidget {
  final String imagePath;

  _ImageDialog(this.imagePath);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500,
      width: double.infinity,
      decoration: ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0))),
      child: Padding(
        padding: const EdgeInsets.all(40.0),
        child: Image.asset(
          imagePath,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class _TextDialog extends StatelessWidget {
  final String dialogText;

  _TextDialog(this.dialogText);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0))),
      child: Padding(
        padding: const EdgeInsets.all(40.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.check_circle_outline,
              color: Colors.green,
              size: 60,
            ),
            Text(dialogText),
          ],
        ),
      ),
    );
  }
}
