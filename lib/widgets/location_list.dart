import 'package:flutter/material.dart';

import '../models/locations.dart';

class LocationList extends StatefulWidget {
  final List<String> locations = locationsList;
  final Function selectLocation;
  var selectedLocation = '';

  LocationList(this.selectLocation);

  @override
  _LocationListState createState() => _LocationListState();
}

class _LocationListState extends State<LocationList> {

  void changeLocation(loc){
    setState(() {
      widget.selectLocation(loc);
      widget.selectedLocation = loc;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new DropdownButton<String>(
        isExpanded: true,
        items: widget.locations.map((String value) {
          return new DropdownMenuItem<String>(
            value: value,
            child: new Text(value),
          );
        }).toList(),
        value: widget.selectedLocation.length ==0?'Hyderabad':widget.selectedLocation,
        onChanged: (value) {
          changeLocation(value);
        },
      ),
    );
  }
}
