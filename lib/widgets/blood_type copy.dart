import 'package:flutter/material.dart';

class BloodType extends StatelessWidget {
  final String bloodType;
  final String selectedType;
  final Function changeBloodType;

  BloodType(this.bloodType, this.selectedType, this.changeBloodType);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black,
        ),
      ),
      child: Material(
        color: bloodType==selectedType?Colors.red:Colors.white,
        child: InkWell(
          onTap: () {
            changeBloodType();
          },
          splashColor: bloodType==selectedType?Colors.white:Colors.red,
          child: Center(
            child: Text(bloodType),
          ),
        ),
      ),
    );
  }
}
