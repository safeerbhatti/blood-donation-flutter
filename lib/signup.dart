import 'package:flutter/material.dart';

import './widgets/custom_shape.dart';
import './widgets/responsive_ui.dart';
import './widgets/textformfield.dart';
import './models/blood_types.dart';
import './models/locations.dart';
import './models/user.dart';
import './models/donor.dart';
import './home.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  double _height;
  double _width;
  double _pixelRatio;
  bool _large;
  bool _medium;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController fullNameController = TextEditingController();
  TextEditingController mobileNumberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String selectedCity;
  String selectedBloodType;

  void _attemptRegister() async {
    if (fullNameController.text.isEmpty) {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text('Please Enter Full Name')));
    } else if (mobileNumberController.text.isEmpty) {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text('Please Enter Mobile Number')));
    } else if (passwordController.text.isEmpty) {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text('Please Enter Password')));
    } else {
      Donor usr = Donor(
        id: '0',
        name: fullNameController.text,
        contact: mobileNumberController.text,
        location: selectedCity,
        bloodType: selectedBloodType,
      );

      await User.registerUser(usr, passwordController.text);
      showDialog(
        context: context,
        builder: (_) => Image.asset(
          'assets/images/after-signup.jpg',
          fit: BoxFit.fill,
        ),
      ).whenComplete(() {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => Home()),
          ModalRoute.withName('/'),
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    return Material(
      child: Scaffold(
        key: _scaffoldKey,
        body: Container(
          height: _height,
          width: _width,
          margin: EdgeInsets.only(bottom: 5),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                clipShape(),
                form(),
                SizedBox(
                  height: _height / 35,
                ),
                button(),
                signInTextRow(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget clipShape() {
    return Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.75,
          child: ClipPath(
            clipper: CustomShapeClipper(),
            child: Container(
              height: _large
                  ? _height / 6
                  : (_medium ? _height / 3.75 : _height / 3.5),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.red[800], Colors.redAccent],
                ),
              ),
            ),
          ),
        ),
        Opacity(
          opacity: 0.5,
          child: ClipPath(
            clipper: CustomShapeClipper2(),
            child: Container(
              height: _large
                  ? _height / 6
                  : (_medium ? _height / 4.25 : _height / 4),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.red[800], Colors.redAccent],
                ),
              ),
            ),
          ),
        ),
        Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(top: 100),
          child: Container(
            margin: EdgeInsets.only(left: _width / 20),
            child: Row(
              children: <Widget>[
                Text(
                  "Sign Up",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 26,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget form() {
    return Container(
      margin: EdgeInsets.only(
          left: _width / 12.0, right: _width / 12.0, top: _height / 20.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            firstNameTextFormField(),
            SizedBox(height: _height / 60.0),
            phoneTextFormField(),
            SizedBox(height: _height / 60.0),
            passwordTextFormField(),
            SizedBox(height: _height / 60.0),
            cityTextFormField(),
            SizedBox(height: _height / 60.0),
            bloodTypeTextFormField(),
          ],
        ),
      ),
    );
  }

  Widget firstNameTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      textEditingController: fullNameController,
      icon: Icons.person,
      hint: "Full Name",
    );
  }

  Widget phoneTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.phone,
      textEditingController: mobileNumberController,
      icon: Icons.phone,
      hint: "Mobile Number (Digits Only)",
    );
  }

  Widget passwordTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      textEditingController: passwordController,
      obscureText: true,
      icon: Icons.lock,
      hint: "Password",
    );
  }

  Widget cityTextFormField() {
    return Material(
      borderRadius: BorderRadius.circular(30.0),
      elevation: 12,
      child: DropdownButtonFormField(
        items: locationsList.map((String city) {
          return new DropdownMenuItem(
            value: city,
            child: Container(
              child: Text(city),
            ),
          );
        }).toList(),
        onChanged: (newValue) {
          selectedCity = newValue;
        },
        value: 'Hyderabad',
        decoration: InputDecoration(
          prefixIcon:
              Icon(Icons.location_city, color: Colors.red[800], size: 20),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30.0),
              borderSide: BorderSide.none),
        ),
      ),
    );
  }

  Widget bloodTypeTextFormField() {
    return Material(
      borderRadius: BorderRadius.circular(30.0),
      elevation: 12,
      child: DropdownButtonFormField(
        items: bloodTypes.map((String type) {
          return new DropdownMenuItem(
            value: type,
            child: Container(
              child: Text(type),
            ),
          );
        }).toList(),
        onChanged: (newValue) {
          selectedBloodType = newValue;
        },
        value: 'A+',
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.favorite, color: Colors.red[800], size: 20),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30.0),
              borderSide: BorderSide.none),
        ),
      ),
    );
  }

  Widget button() {
    return RaisedButton(
      elevation: 0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      onPressed: () {
        _attemptRegister();
      },
      textColor: Colors.white,
      padding: EdgeInsets.all(0.0),
      child: Container(
        alignment: Alignment.center,
        width: _large ? _width / 4 : (_medium ? _width / 3.75 : _width / 3.5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          gradient: LinearGradient(
            colors: <Color>[Colors.red[800], Colors.redAccent],
          ),
        ),
        padding: const EdgeInsets.all(12.0),
        child: Text(
          'SIGN UP',
          style: TextStyle(fontSize: _large ? 14 : (_medium ? 12 : 10)),
        ),
      ),
    );
  }

  Widget signInTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Already have an account?",
            style: TextStyle(fontWeight: FontWeight.w400),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
              print("Routing to sign in screen");
            },
            child: Text(
              "Sign in",
              style: TextStyle(
                  fontWeight: FontWeight.w800,
                  color: Colors.red[800],
                  fontSize: 19),
            ),
          )
        ],
      ),
    );
  }
}
