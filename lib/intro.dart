import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';

import './signin.dart';
import './models/user.dart';

class IntroSlides extends StatefulWidget {
  @override
  _IntroSlidesState createState() => _IntroSlidesState();
}

class _IntroSlidesState extends State<IntroSlides> {
  List<Slide> slides = new List();

  @override
  void initState() {
    super.initState();

    User.isbyPassScreens().then((isSkip) {
      if (isSkip) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => SignInPage()),
          ModalRoute.withName('/login'),
        );
      }
    });

    slides.add(
      new Slide(
        backgroundImage: 'assets/images/screen-1.jpg',
        backgroundImageFit: BoxFit.fill,
        backgroundBlendMode: BlendMode.screen,
      ),
    );

    slides.add(
      new Slide(
        backgroundImage: 'assets/images/screen-2.jpg',
        backgroundImageFit: BoxFit.fill,
        backgroundBlendMode: BlendMode.screen,
      ),
    );

    slides.add(
      new Slide(
        backgroundImage: 'assets/images/screen-3.jpg',
        backgroundImageFit: BoxFit.fill,
        backgroundBlendMode: BlendMode.screen,
      ),
    );
  }

  void onDonePress(ctx) {
    User.byPassScreens();
    Navigator.pushAndRemoveUntil(
      ctx,
      MaterialPageRoute(builder: (BuildContext context) => SignInPage()),
      ModalRoute.withName('/login'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: IntroSlider(
        slides: this.slides,
        onDonePress: () {
          onDonePress(context);
        },
        colorActiveDot: Colors.red,
        colorDot: Colors.black,
        colorDoneBtn: Colors.red,
        colorPrevBtn: Colors.red,
        colorSkipBtn: Colors.redAccent,
      ),
    );
  }
}
