import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

import './intro.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Blood Donation',
      theme: ThemeData(
        primarySwatch: Colors.red,
        accentColor: Colors.white,
        fontFamily: 'Heebo',
        textTheme: ThemeData.light().textTheme.copyWith(
              body1: TextStyle(
                fontFamily: 'Heebo',
              ),
              title: TextStyle(
                fontFamily: 'Heebo',
                fontSize: 42,
              ),
            ),
      ),
      home: SplashScreen(
        seconds: 5,
        navigateAfterSeconds:IntroSlides(),
        image: new Image.asset('assets/images/logo.png'),
        backgroundColor: Colors.white,
        photoSize: 100.0,
        loaderColor: Colors.red,
      ),
    );
  }
}
