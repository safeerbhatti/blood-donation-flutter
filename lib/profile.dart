import 'package:flutter/material.dart';

import './models/donor.dart';
import './models/user.dart';
import 'package:http/http.dart' as http;

class Profile extends StatelessWidget {
  Donor usr;

  Future<Map> _getProfile() async {
    usr = await User.getUser();
    const url = 'http://aieshs.com/blood_donation/getProfile.php';
    var response = await http.post(url, body: {
      'uid': usr.id,
    });

    if (response.statusCode == 200) {
      return {'test': 'test'};
    } else {
      throw Exception('Failed to get Profile');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 15, left: 15, right: 10),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.black12,
                  ),
                ),
              ),
              alignment: Alignment.centerLeft,
              child: Text(
                'My Profile',
                style: TextStyle(fontSize: 42),
              ),
            ),
            FutureBuilder<Map>(
              future: _getProfile(),
              builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
                Widget children;

                if (snapshot.hasData) {
                  children = Expanded(
                    child: _ProfileView(usr, snapshot.data),
                  );
                } else {
                  children = SizedBox(
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                    ),
                    width: 60,
                    height: 60,
                  );
                }
                return Container(
                  child: children,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class _ProfileView extends StatelessWidget {
  final profileData;
  final Donor usr;

  _ProfileView(this.usr, this.profileData);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _Header(usr),
      ],
    );
  }
}

class _Header extends StatelessWidget {
  final Donor usr;

  _Header(this.usr);

  TextStyle defaultStyle = TextStyle(
    fontWeight: FontWeight.w500,
    color: Colors.red,
    fontSize: 16,
  );

  TextStyle labelStyle = TextStyle(
    fontSize: 14,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Container(
                decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.black54)),
                ),
                child: Text(
                  usr.name,
                  style: TextStyle(fontSize: 26),
                ),
              ),
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10.0),
              decoration:
                  BoxDecoration(border: Border.all(color: Colors.black54)),
              child: Column(
                children: <Widget>[
                  Text('4', style: defaultStyle),
                  Text('Total Donations', style: labelStyle),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              decoration:
                  BoxDecoration(border: Border.all(color: Colors.black54)),
              child: Column(
                children: <Widget>[
                  Text('4', style: defaultStyle),
                  Text('Last Donation', style: labelStyle),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
