import 'package:flutter/material.dart';

import './models/donor.dart';
import './models/user.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class History extends StatelessWidget {
  Future<List> _getDonors() async {
    Donor usr = await User.getUser();
    const url = 'http://aieshs.com/blood_donation/getHistory.php';
    var response = await http.post(url, body: {
      'uid': usr.id,
    });

    if (response.statusCode == 200) {
      return json
          .decode(response.body)
          .map((data) => Donor.fromJson(data))
          .toList();
    } else {
      throw Exception('Failed to get history');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 15, left: 15, right: 10),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.black12,
                  ),
                ),
              ),
              alignment: Alignment.centerLeft,
              child: Text(
                'My History',
                style: TextStyle(fontSize: 42),
              ),
            ),
            FutureBuilder<List>(
              future: _getDonors(),
              builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                Widget children;

                if (snapshot.hasData) {
                  children = Expanded(
                    child: _DonorsList(snapshot.data),
                  );
                } else {
                  children = SizedBox(
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                    ),
                    width: 60,
                    height: 60,
                  );
                }
                return Container(
                  child: children,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class _DonorsList extends StatelessWidget {
  final List<dynamic> _donorsList;

  _DonorsList(this._donorsList);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _donorsList.length,
      itemBuilder: (BuildContext context, int index) {
        Donor dnr = _donorsList[index];
        return Card(
          elevation: 5,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                  bottom: 10,
                  left: 20,
                  right: 20,
                ),
                height: 70,
                width: 70,
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    border: Border.all(color: Colors.white54)),
                child: Center(
                  child: Text(
                    dnr.bloodType,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 26,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Center(
                            child: Text(
                              dnr.name,
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            child: Text('City: ${dnr.location}'),
                          ),
                          Container(
                            child: Text('Date: ${dnr.onDate}'),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            child: Text('Status: ${dnr.status==1?'Pending':'Completed'}'),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
