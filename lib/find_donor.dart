import 'package:flutter/material.dart';

import './donors.dart';
import './find_donor_form.dart';

class FindDonor extends StatefulWidget {
  @override
  _FindDonorState createState() => _FindDonorState();
}

class _FindDonorState extends State<FindDonor> {
  var selectedWindow = 0;
  var selectedBloodType;
  var selectedLocation = "Hyderabad";
  var searchType;

  void selectBloodType(var type) {
    selectedBloodType = type;
  }

  void selectLocation(String location) {
    selectedLocation = location;
  }

  void goBack() {
    setState(() {
      selectedBloodType = null;
      selectedWindow = 0;
    });
  }

  void validateForm(String type) {
    if (selectedBloodType != null) {
      setState(() {
        searchType = type;
        selectedWindow = 1;
      });
    } else {
      AlertDialog _showDialog = AlertDialog(
        title: Text('Error'),
        content: Text('Please Select Blood Type'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text('Okay'),
          )
        ],
      );

      showDialog(context: context, child: _showDialog);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedContainer(
        duration: Duration(seconds: 3),
        curve: Curves.fastOutSlowIn,
        child: selectedWindow == 0
            ? FindDonorForm(selectBloodType, selectLocation, validateForm)
            : Donors(
                goBack: goBack,
                bloodType: selectedBloodType,
                searchLocation: selectedLocation,
                searchType: searchType,
              ),
      ),
    );
  }
}
