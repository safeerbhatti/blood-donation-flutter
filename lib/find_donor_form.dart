import 'package:flutter/material.dart';

import './widgets/blood_type_list.dart';
import './widgets/location_list.dart';

class FindDonorForm extends StatefulWidget {
  final Function selectBloodType;
  final Function selectLocation;
  final Function validateForm;

  FindDonorForm(this.selectBloodType, this.selectLocation, this.validateForm);

  @override
  _FindDonorFormState createState() => _FindDonorFormState();
}

class _FindDonorFormState extends State<FindDonorForm> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 15, left: 10, right: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Find Donor',
              style: TextStyle(fontSize: 36, fontWeight: FontWeight.w700),
            ),
            Text(
              'Search for blood donors around You',
              style: TextStyle(
                fontSize: 12,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Text(
                'Choose Blood Group',
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.start,
              ),
            ),
            BloodTypeList(widget.selectBloodType),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  Text(
                    'Location',
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
            ),
            LocationList(widget.selectLocation),
            Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 30),
                child: Row(
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: (MediaQuery.of(context).size.width/2)-20,
                      child: RaisedButton(
                        color: Colors.red,
                        textColor: Colors.white,
                        padding: EdgeInsets.all(20),
                        child: Center(
                          child: Text('Search Donor'),
                        ),
                        onPressed: () {
                          widget.validateForm('normal');
                        },
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    ButtonTheme(
                      minWidth: (MediaQuery.of(context).size.width/2)-20,
                      child: RaisedButton(
                        color: Colors.red,
                        textColor: Colors.white,
                        padding: EdgeInsets.all(20),
                        child: Center(
                          child: Text('Search Nearby'),
                        ),
                        onPressed: () {
                          widget.validateForm('nearby');
                        },
                      ),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
