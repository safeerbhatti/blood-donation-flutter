import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import './donor.dart';

class User{

  static var isLogged = false;

  static Future<Donor> getUser() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return Donor.fromJson(json.decode(prefs.getString('user')));
  }

  static Future<bool> loginUser(username, password) async{
    const url = 'http://aieshs.com/blood_donation/login.php';
    var response = await http.post(url, body: {
      'username': username,
      'password': password,
    });

    if (response.statusCode == 200) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var rt = await prefs.setString('user', response.body);
      if(rt) isLogged = true;
      return isLogged;
    } 
    else if(response.statusCode == 201) return false; 
    else throw Exception('Failed to login http request');
    
  }

  static Future<bool> registerUser(Donor usr, String password) async{
    var usrJson = json.encode(usr.toJson());

    const url = 'http://aieshs.com/blood_donation/register.php';
    var response = await http.post(url, body: {
      'user': usrJson,
      'password': password,
    });

    if (response.statusCode == 200) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var rt = await prefs.setString('user', usrJson);
      if(rt) isLogged = true;
      return isLogged;
    } 
    else if(response.statusCode == 201) return false; 
    else throw Exception('Failed to register http request');
  }

  static Future<bool> logout(Donor usr) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('user');
    return isLogged = false;
  } 

  static Future<bool> byPassScreens() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setBool('byPassScreens', true);
  }

  static Future<bool> isbyPassScreens() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      return prefs.getBool('byPassScreens');
    } catch (e) {
      return false;
    }
  }
}