import 'package:flutter/foundation.dart';

class Donor{
  String id;
  String name;
  String bloodType;
  String location;
  String contact;
  String onDate;
  String donorName;
  int status; // 0 = pending, 1 = accept, 2 = donated

  Donor({
    @required this.id,
    @required this.name,
    @required this.bloodType,
    @required this.location,
    @required this.contact,
    this.onDate,
    this.donorName,
    this.status
  });

  factory Donor.fromJson(Map<String, dynamic> parsedJson){
    return Donor(
      id: parsedJson['id'],
      name : parsedJson['name'],
      bloodType : parsedJson['bloodtype'],
      location : parsedJson['city'],
      contact : parsedJson['mobile_number'],
      onDate : parsedJson['ondate'] ?? '',
      donorName : parsedJson['donor'] ?? '',
      status : parsedJson['status'] == null?0:num.parse(parsedJson['status']),
    );
  }

  Map<String, dynamic> toJson() => {
    'id' : id,
    'name': name,
    'bloodType': bloodType,
    'location': location,
    'contact': contact,
  };
}